﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
  
    public WayPoints currentWayPoint;
    [Space]
    public Transform CheckForwardPosition;
    public Transform CheckRightPosition;
    public Transform CheckLeftPosition;
    [Space]
    public float maxSpeed =1;
    public float rotationSpeed;

    public Coroutine accelerator;
    [HideInInspector]
    public Transform myTransform;  
    [HideInInspector]
    public float currentSpeed=1;
    [HideInInspector]
    public Transform target;
    [HideInInspector]
    public bool carInFront;
    [HideInInspector]
    public bool CheckCarRight;
    [HideInInspector]
    public bool CheckCarLeft;

    void Start()
    {
        if(myTransform==null)  myTransform = GetComponent<Transform>();
        myTransform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color((float)Random.Range(0f, 1f), (float)Random.Range(0f, 1f), (float)Random.Range(0f, 1f));
        target = currentWayPoint.transform;
        Invoke("ReParent", 1f);
    }

    public void ReParent()
    {
        myTransform.parent = null;
    }
   
    void Update()
    {
        Move();
        LookForward();
    }

    public void Move()
    {
        myTransform.position = Vector3.MoveTowards(myTransform.position, target.position, currentSpeed * Time.deltaTime);
       
        if( Approximate())
        {
            ChooseWay();
        }
        if(carInFront && !WayClose())
        {
            carInFront = false;
            if (accelerator != null)
            {
                StopCoroutine(accelerator);
            }
            accelerator = StartCoroutine(Acceleration(maxSpeed));
        }
    }

    public bool Approximate()
    {
        if(myTransform.position.x>target.position.x-0.3f&& myTransform.position.x < target.position.x + 0.3f)
        {
            if(myTransform.position.y > target.position.y - 0.3f && myTransform.position.y < target.position.y + 0.3f)
            {
                return true;
            }
        }
        return false;
    }

    public IEnumerator Acceleration(float speed)
    {
        float tmp;
        if(speed ==0)
        {
            tmp = -maxSpeed/4;
        }
        else
        {
            tmp = maxSpeed / 10f;
        }
        while(tmp >0 && currentSpeed <maxSpeed || tmp<0 && currentSpeed > maxSpeed / 5f)
        {
            currentSpeed += tmp;
            yield return new WaitForSeconds(0.1f);

        }
        currentSpeed = speed;
        accelerator = null;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    { 
        if (collision.gameObject.tag =="Car" && !collision.isTrigger )
        {
              carInFront = true;
            if(accelerator!= null)
            {
                StopCoroutine(accelerator);
            }
            accelerator = StartCoroutine(Acceleration(0));
        }
       
    }
    public void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Car" && !WayClose())
        {
           carInFront = false;
            if (accelerator != null)
            {
                StopCoroutine(accelerator);
            }
            accelerator = StartCoroutine(Acceleration(maxSpeed));
        }
    }

    public void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Car" && !collision.isTrigger)
        {
            carInFront = true;
            if (accelerator != null)
            {
                StopCoroutine(accelerator);
            }
            currentSpeed = 0;
        }
    }

    

    private bool WayClose()
    {
        Collider2D [] ObjectsOnWay = Physics2D.OverlapCircleAll(CheckForwardPosition.position, 0.05f);
        int j = 0;
        for (int i = 0; i < ObjectsOnWay.Length; i++)
        {
            if (ObjectsOnWay[i].gameObject != gameObject && ObjectsOnWay[i].gameObject.tag =="Car")
                j++;

        }
      
        return j > 0;
        
    }
   
    public bool CheckRight()
    {
        Collider2D[] ObjectsOnWay = Physics2D.OverlapCircleAll(CheckRightPosition.position, 0.2f);
        
        int j = 0;
        for (int i = 0; i < ObjectsOnWay.Length; i++)
        {
            if (ObjectsOnWay[i].gameObject != this.gameObject && ObjectsOnWay[i].gameObject.tag == "Car")
            { 
                j++;
            }

        } 
    
        return j > 0;
    }
    public bool CheckLeft()
    {
        Collider2D[] ObjectsOnWay = Physics2D.OverlapCircleAll(CheckLeftPosition.position, 0.2f);

        int j = 0;
        for (int i = 0; i < ObjectsOnWay.Length; i++)
        {
            if (ObjectsOnWay[i].gameObject != this.gameObject && ObjectsOnWay[i].gameObject.tag == "Car")
            {
                j++;
            }

        }

        return j > 0;
    }
    public void LookForward()
    {
        if (myTransform.position != target.position)
        {
            Vector3 direction;
            direction = target.transform.position - transform.position;
            direction.Normalize();

            float rot = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion rightRot = Quaternion.Euler(new Vector3(0, 0, rot));
            
            Rotate(rightRot);

        }
    }

    public void Rotate(Quaternion rightRot)
    {
        myTransform.rotation = Quaternion.Lerp(this.transform.rotation, rightRot, Time.deltaTime * rotationSpeed);
    }
  
    public void ChooseWay()
    {
        if (currentWayPoint.freeForMove)
        {
            if (!CheckCarRight && !CheckCarLeft|| CheckCarRight && !CheckRight() || CheckCarLeft && !CheckLeft())
            {
                CheckCarRight = false;
                CheckCarLeft = false;
                currentWayPoint = currentWayPoint.GetAvailableWay();
                target = currentWayPoint.gameObject.transform;
            }
            if (currentWayPoint.NeedCheckRight) CheckCarRight = true;
            if (currentWayPoint.NeedCheckLeft) CheckCarLeft = true;
        }
    }
}
