﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Other : MonoBehaviour
{  
    // Для тестування
    public Slider TimeScaleSlider;
    public void TimeScale()
    {
        Time.timeScale = TimeScaleSlider.value;
    }
}
