﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour
{
    public bool trafficControl;
    public WayPoints [] traficLight;
    [Space]

    [Header("For connector:")]

    public WayPoints[] DownPoints;
    public WayPoints[] TopPoints;
    public WayPoints[] LeftPoints;
    public WayPoints[] RightPoints;
    [Space]
    public Road Left;
    public Road Right;
    public Road Top;
    public Road Down;
    void Start()
    {
        for (int i = 0; i < FindObjectsOfType<Road>().Length; i++)
        {
            FindObjectsOfType<Road>()[i].Connect();
        }
        
        if (trafficControl)    StartCoroutine(TrafficLightTimer());
    }

    public IEnumerator TrafficLightTimer()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(5, 10));
            traficLight[0].LightState(0);
            traficLight[1].LightState(3);
            traficLight[2].LightState(0);
            traficLight[3].LightState(3);
            yield return new WaitForSeconds(Random.Range(5, 10));
            traficLight[0].LightState(1);
            traficLight[1].LightState(3);
            traficLight[2].LightState(1);
            traficLight[3].LightState(3);
            yield return new WaitForSeconds(Random.Range(5, 10));
            traficLight[0].LightState(2);
            traficLight[1].LightState(3);
            traficLight[2].LightState(2);
            traficLight[3].LightState(3);
            yield return new WaitForSeconds(Random.Range(5, 10));
            traficLight[0].LightState(3);
            traficLight[1].LightState(0);
            traficLight[2].LightState(3);
            traficLight[3].LightState(0);
            yield return new WaitForSeconds(Random.Range(5, 10));
            traficLight[0].LightState(3);
            traficLight[1].LightState(1);
            traficLight[2].LightState(3);
            traficLight[3].LightState(1);
            yield return new WaitForSeconds(Random.Range(5, 10));
            traficLight[0].LightState(3);
            traficLight[1].LightState(2);
            traficLight[2].LightState(3);
            traficLight[3].LightState(2);
            
        }
    }


    public void Connect()
    {
        if (Left != null)
        {
            Left.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x - (4.4f*1.5f), this.gameObject.transform.position.y, 0);
            LeftPoints[0].availableWayPoints = new WayPoints[1];
            LeftPoints[0].availableWayPoints[0] = Left.RightPoints[0];
            Left.RightPoints[1].availableWayPoints = new WayPoints[1];
            Left.RightPoints[1].availableWayPoints[0] = LeftPoints[1];
        }
        if (Right != null)
        {
            Right.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x + (4.4f * 1.5f), this.gameObject.transform.position.y, 0);
            RightPoints[1].availableWayPoints = new WayPoints[1];
            RightPoints[1].availableWayPoints[0] = Right.LeftPoints[1];
            Right.LeftPoints[0].availableWayPoints = new WayPoints[1];
            Right.LeftPoints[0].availableWayPoints[0] = RightPoints[0];
        }
        if (Top != null)
        {
            Top.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x , this.gameObject.transform.position.y+ (4.4f * 1.5f), 0);
            TopPoints[0].availableWayPoints = new WayPoints[1];
            TopPoints[0].availableWayPoints[0] = Top.DownPoints[0];
            Top.DownPoints[1].availableWayPoints = new WayPoints[1];
            Top.DownPoints[1].availableWayPoints[0] = TopPoints[1];
        }
        if (Down != null)
        {
            Down.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y - (4.4f * 1.5f), 0);
            DownPoints[1].availableWayPoints = new WayPoints[1];
            DownPoints[1].availableWayPoints[0] = Down.TopPoints[1];
            Down.TopPoints[0].availableWayPoints = new WayPoints[1];
            Down.TopPoints[0].availableWayPoints[0] = DownPoints[0];
        }
    }
}
