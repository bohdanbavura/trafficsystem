﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoints : MonoBehaviour
{
    public WayPoints[] availableWayPoints;
    [Space]
    public bool TrafficLight;
    public GameObject[] TrafficLights;
    [Space]
    public bool NeedCheckRight;
    public bool NeedCheckLeft;
    [HideInInspector]
    public bool freeForMove = true;


    public void Start()
    {
        if (TrafficLight)
        {
            freeForMove = false; 
        }
    }
     
    public void LightState(int state)
    {
        switch (state)
        {
            case 0:
                TrafficLights[0].SetActive(false);
                TrafficLights[1].SetActive(true);
                TrafficLights[2].SetActive(false);
                break;
            case 1:
                TrafficLights[0].SetActive(false);
                TrafficLights[1].SetActive(false);
                TrafficLights[2].SetActive(true);
                freeForMove = true;
                break;
            case 2:
                TrafficLights[0].SetActive(false);
                TrafficLights[1].SetActive(true);
                TrafficLights[2].SetActive(false);
                freeForMove = false;
                break;
            case 3:
                TrafficLights[0].SetActive(true);
                TrafficLights[1].SetActive(false);
                TrafficLights[2].SetActive(false);
               // freeForMove = false;
                break;
        }
    }

    public WayPoints GetAvailableWay()
    {
        return availableWayPoints[Random.Range(0, availableWayPoints.Length)];
    }
    
}
